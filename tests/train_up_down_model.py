from data_api import get_data_for_training
from configuration import binance_client
from utils.model.up_down_utils import train_up_down_model


get_data_for_training(binance_client.PAIR, binance_client.START_TIME, binance_client.END_TIME, binance_client.INTERVAL, binance_client.API_KEY, binance_client.API_SECRET)
train_up_down_model(binance_client.PAIR)
