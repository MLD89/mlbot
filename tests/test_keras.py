import os
from keras.models import load_model

from utils.model.model_utils import load_data, plot_results

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
model_json_path = "model/BNBBTC_model*.json"
model_path = "model/BNBBTC_model.h5"
data_file_path = 'data/BNBBTC-past-6h.csv'

testX, testY, scaler = load_data(data_file_path, _from_file=True)
model = load_model(model_path)

predictions = model.predict(testX)
plot_results(predictions, testY, scaler)
