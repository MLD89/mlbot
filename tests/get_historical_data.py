import csv

from binance.client import Client

from utils.date.date import get_date_from_utc
from statics.headers import headers

CSV_FILE_PATH = 'data/BNBBTC-past-6h.csv'
HEADERS = headers
csv_file = open(CSV_FILE_PATH, "a")
writer = csv.DictWriter(csv_file, headers)
writer.writeheader()
csv_file.flush()
API_KEY = 'ujiL8jglEw1B0VEweUayK4VLKjy6JIOWE76o3TIHxtiexzLNnjVpi68nt4CbxSSx'
api_secret = '1EnkzgiSrcjLjbZGkzGFfc1uL4TKq7qxLO30UzFnjc4QBtAlviIT39Ch81ws4zlU'

client = Client(API_KEY, api_secret)

candles = client.get_historical_klines('BNBBTC', Client.KLINE_INTERVAL_1MINUTE, "6 hour ago UTC")

with open(CSV_FILE_PATH, "a") as _csv_file:
    for row in candles:
        open_date, open_time = get_date_from_utc(row[0])

        row.insert(1, open_date)
        row.insert(2, open_time)

        close_date, close_time = get_date_from_utc(row[8])
        row.insert(9, close_date)
        row.insert(10, close_time)

        wr = csv.writer(_csv_file, dialect='excel')
        wr.writerow(row)
        _csv_file.flush()
