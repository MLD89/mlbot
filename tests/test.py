import matplotlib.pyplot as plt
import torch

from network.configuration import opt
from network.loader import DataLoader
from network.network import Network

train_data = DataLoader('data/BNBBTC-past-6h.csv', opt.look_back)
train_loader = torch.utils.data.DataLoader(train_data, batch_size=1)

net = Network(opt.look_back, 32, num_layers=opt.layers)
net.load_state_dict(torch.load('model/torch.pth'))
net.eval()

outputs = []
reals = []
for each, beach in train_loader:
    with torch.no_grad():
        output = net(each)
        outputs.append(output[0][0][0].item() / 100)
        reals.append(beach[0][0][0].item() / 100)

plt.plot(outputs)
# plt.plot(reals)
plt.show()
