import os

from model.init_model import MODEL

from network.configuration import opt
from utils.data import get_past_candlesticks
from utils.model.model_utils import load_data, predict_sequences_multiple
from utils.structures import Stack
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

result, times = get_past_candlesticks('BNBBTC', opt.look_back)

LOOK_BACK_LIST = Stack(result)


predictions = []
i = 0

result, _minutes = get_past_candlesticks('BNBBTC', opt.look_back)
_static_stack = Stack(result)

while i < 15:
    _input, scaler = load_data(_static_stack.items, _from_list=True)
    i += 1
    prediction = predict_sequences_multiple(MODEL, _input, scaler)[0][0]
    predictions.append(prediction)
    _static_stack.push(prediction)

print(len(predictions))