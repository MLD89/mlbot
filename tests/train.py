import torch
from network.configuration import opt
from network.loader import DataLoader
from network.network import Network
torch.set_printoptions(precision=15)

train_data = DataLoader('data/BNBBTC-7-8.csv', opt.look_back)
train_loader = torch.utils.data.DataLoader(train_data, batch_size=opt.batch_size)

net = Network(opt.look_back, 32, num_layers=opt.layers)

criterion = torch.nn.L1Loss()
optimizer = torch.optim.Adam(net.parameters(), lr=1e-3)
for _index in range(opt.epochs):
    for train_x, train_y in train_loader:
        # print(train_x)
        # break
        optimizer.zero_grad()
        out = net(train_x)
        print(out)
        # print(out)
        loss = criterion(out, train_y)
        loss.backward()
        optimizer.step()
    # break
    if (_index + 1) % 1 == 0:
        print('Epoch: {}, Loss: {:.5f}'.format(_index + 1, loss.item()))

torch.save(net.state_dict(), 'model/torch.pth')
