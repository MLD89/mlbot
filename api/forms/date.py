from wtforms import Form, StringField, validators


class InsertDateForm(Form):
    start_date = StringField('Starting date:', validators=[validators.required()])
    end_date = StringField('Ending date:', validators=[validators.required()])
