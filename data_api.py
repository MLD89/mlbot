import os
import pandas
from flask import render_template, flash, request, send_from_directory, url_for, redirect, Response
from talib import STOCHF, STOCH, BBANDS
from api.forms.date import InsertDateForm
from utils.date.date import get_date_from_utc, check_date
from statics.symbols import symbols, intervals
from utils.data.data_calculation import *
from statics.headers import headers
from configuration import BinanceModel, ApiModel

app = ApiModel().get_app()


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')


@app.route('/<pair>/<start_date>/<end_date>/<interval>', methods=['GET'])
def get_data(pair, start_date, end_date, interval):
    _binance_client = BinanceModel()
    _client = _binance_client.get_client()
    klines = _client.get_historical_klines(pair, intervals[interval], start_date, end_date)
    rows = []
    ATR_object = ATR()
    for _index, row in enumerate(klines):
        open_date, open_time = get_date_from_utc(row[0])
        row.insert(1, open_date)
        row.insert(2, open_time)
        # Cast in float close prices
        row[6] = float(row[6])
        row[5] = float(row[5])
        row[4] = float(row[4])
        row[3] = float(row[3])
        row[7] = float(row[7])

        close_date, close_time = get_date_from_utc(row[8])
        row.insert(9, close_date)
        row.insert(10, close_time)

        close = rows[_index - 1][6] if _index > 0 else 0
        current_true_range = true_range(float(row[4]), float(row[5]), float(close))
        current_ATR = ATR_object.current_atr(current_true_range)
        row.append(current_true_range)
        row.append(current_ATR)
        rows.append(row)

    dataframe = pandas.DataFrame(rows, columns=headers)
    if dataframe.empty:
        flash('Error: No data from binance with those parameters')
        form = InsertDateForm(request.form)
        sorted_symbols = sorted(symbols)
        return render_template('index.html', form=form, pairs=sorted_symbols, intervals=intervals)

    dataframe.fillna(float(0))
    dataframe['ROC_1'] = ROC(dataframe, timeperiod=2, price='close')
    dataframe['ROC_5'] = ROC(dataframe, timeperiod=5, price='close')
    dataframe['ROC_20'] = ROC(dataframe, timeperiod=20, price='close')
    dataframe['ROC_50'] = ROC(dataframe, timeperiod=50, price='close')
    dataframe['ROC_150'] = ROC(dataframe, timeperiod=150, price='close')
    dataframe["SMA_10"] = SMA(dataframe, timeperiod=10, price='close')
    dataframe["SMA_20"] = SMA(dataframe, timeperiod=20, price='close')
    dataframe["SMA_50"] = SMA(dataframe, timeperiod=50, price='close')
    dataframe["RSI"] = RSI(dataframe, timeperiod=14, price='close')
    dataframe["Williams"] = WILLR(dataframe, timeperiod=14, price='close')
    dataframe['CLV'] = get_clv(dataframe)
    dataframe['ADI'] = get_adi(dataframe)

    dataframe.fillna(0)
    dataframe['EMA_20'] = EMA(dataframe, timeperiod=20, price='close')
    dataframe['EMA_50'] = EMA(dataframe, timeperiod=50, price='close')
    dataframe['CCI_40'] = CCI(dataframe, timeperiod=40, price='close')
    dataframe['ROC_RSI'] = ROC(dataframe, timeperiod=5, price='RSI')
    dataframe['ROC_SMA_20'] = ROC(dataframe, timeperiod=5, price='SMA_20')
    dataframe['ROC_SMA_50'] = ROC(dataframe, timeperiod=5, price='SMA_50')
    dataframe['ROC_EMA_20'] = ROC(dataframe, timeperiod=5, price='EMA_20')
    dataframe['ROC_EMA_50'] = ROC(dataframe, timeperiod=5, price='EMA_50')
    dataframe['ROC_EMA_CCI'] = ROC(dataframe, timeperiod=5, price='CCI_40')
    dataframe['ROC_VOLUME_1'] = ROC(dataframe, timeperiod=2, price='Volume')
    dataframe['ROC_VOLUME_5'] = ROC(dataframe, timeperiod=5, price='Volume')
    dataframe['ROC_WILLIAMS'] = ROC(dataframe, timeperiod=5, price='Williams')
    dataframe['ROC_ADI'] = ROC(dataframe, timeperiod=5, price='ADI')
    dataframe['EMA_12'] = EMA(dataframe, timeperiod=12, price='close')
    dataframe['EMA_26'] = EMA(dataframe, timeperiod=26, price='close')
    dataframe['MACD'] = dataframe['EMA_26'] - dataframe['EMA_12']
    dataframe['MACD_signal'] = EMA(dataframe, 9, price='MACD')
    dataframe['ROC_MACD'] = ROC(dataframe, timeperiod=5, price='MACD')
    dataframe['ROC_MACD_signal'] = ROC(dataframe, timeperiod=5, price='MACD_signal')
    dataframe['AROONOSC'] = AROONOSC(dataframe, timeperiod=25, prices=['high', 'low'])
    dataframe['ROC_AROONOSC'] = ROC(dataframe, timeperiod=5, price='AROONOSC')
    fastk, _ = STOCHF(dataframe['high'].values, dataframe['low'].values, dataframe['close'].values,
                      fastk_period=20, fastd_period=3)
    dataframe['FSO'] = fastk
    fastk, _ = STOCHF(dataframe['high'].values, dataframe['low'].values, dataframe['close'].values,
                      fastk_period=20, fastd_period=3)
    dataframe['STOCFK_3'] = fastk
    dataframe['FSI'] = EMA(dataframe, timeperiod=3, price='STOCFK_3')
    dataframe['ROC_FSO'] = ROC(dataframe, timeperiod=5, price='FSO')
    dataframe['ROC_FSI'] = ROC(dataframe, timeperiod=5, price='FSI')

    slowk, _ = STOCH(dataframe['high'].values, dataframe['low'].values, dataframe['close'].values,
                     slowk_period=14, slowd_period=3)
    dataframe['SSO'] = slowk
    slowk, _ = STOCH(dataframe['high'].values, dataframe['low'].values, dataframe['close'].values,
                     slowk_period=14, slowd_period=3)
    dataframe['STOC_3'] = slowk
    dataframe['SSI'] = EMA(dataframe, timeperiod=3, price='STOC_3')
    dataframe['ROC_SSO'] = ROC(dataframe, timeperiod=5, price='SSO')
    dataframe['ROC_SSI'] = ROC(dataframe, timeperiod=5, price='SSI')
    dataframe['SAR'] = get_sar(dataframe)
    up, _, low = BBANDS(dataframe['SMA_20'].values, nbdevup=2, nbdevdn=2)
    dataframe['BBP_SMA_20_UP(2, 2)'] = up
    dataframe['BBP_SMA_20_LOW(2, 2)'] = low
    up, _, low = BBANDS(dataframe['SMA_20'].values, nbdevup=1, nbdevdn=0)
    dataframe['BBP_SMA_20_UP(0, 1)'] = up
    dataframe['BBP_SMA_20_LOW(0, 1)'] = low
    up, _, low = BBANDS(dataframe['EMA_20'].values, nbdevup=2, nbdevdn=2)
    dataframe['BBP_EMA_20_UP(2, 2)'] = up
    dataframe['BBP_EMA_20_LOW(2, 2)'] = low
    up, _, low = BBANDS(dataframe['EMA_20'].values, nbdevup=1, nbdevdn=0)
    dataframe['BBP_EMA_20_UP(0, 1)'] = up
    dataframe['BBP_EMA_20_LOW(0, 1)'] = low

    file_name = "attachment; filename={}.csv".format(pair)
    return Response(dataframe.to_csv(), mimetype="text/csv",
                    headers={"Content-disposition": file_name})


@app.route("/", methods=['GET', 'POST'])
def main():
    form = InsertDateForm(request.form)
    sorted_symbols = sorted(symbols)
    if request.method == 'POST':
        start_date = request.form['start_date']
        end_date = request.form['end_date']
        pair = request.form['pairs']
        interval = request.form['interval']
        if start_date and end_date and pair and interval:
            start_date = check_date(start_date)
            end_date = check_date(end_date)
            if start_date and end_date:
                flash('Sending request. It may take up to 30 minutes, due to long request interval')
                return redirect(
                    url_for('get_data', pair=pair, start_date=start_date, end_date=end_date, interval=interval))
            else:
                flash('Error: Please enter the time correctly. e.g 15/10/2018')
        else:
            flash('Error: All the form fields are required. ')
    return render_template('index.html', form=form, pairs=sorted_symbols, intervals=intervals)


if __name__ == "__main__":
    binance_client = BinanceModel()
    client = binance_client.get_client()
    # print(client.get_historical_klines('BCNBNB', '1m', '10 October, 2018', '11 October, 2018'))
    app.run(host='0.0.0.0', port=6998, debug=True)
