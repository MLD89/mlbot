class Stack:
    def __init__(self, _start_list):
        self.items = _start_list

    def push(self, item):
        self.items.append(item)
        self.items.pop(0)

