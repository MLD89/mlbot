import numpy
from keras.models import load_model

from configuration import BinanceModel
from statics.symbols import intervals
from utils.model.model_utils import load_data
from utils.structures import Stack


def get_predictions(interval, start_time, end_time, pair, PRICE_MODEL=None, GRAPH=None):

    binance_client = BinanceModel()
    loock_back = binance_client.LOOK_BACK
    _client = binance_client.get_client()

    klines = _client.get_historical_klines(pair, intervals[interval], start_time, end_time)
    results = []
    for index, row in enumerate(klines):
        current_input = numpy.array([_row[4] for _row in klines[index:index + loock_back]]).astype('float32')

        if len(current_input) < loock_back or index + loock_back == len(klines) - 1:
            break

        real = float(klines[index + loock_back+1][4])
        _static_stack = Stack(current_input)
        _input, scaler = load_data(_static_stack.items, _from_list=True)
        with GRAPH.as_default():
            prediction_seqs = PRICE_MODEL.predict(_input)
        prediction = scaler.inverse_transform(prediction_seqs)[0][0]
        results.append([real, prediction, abs(prediction-real)])

    return results

