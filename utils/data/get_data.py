from utils.data.data_calculation import *
from statics.symbols import intervals
from statics.headers import headers
import pandas
from configuration import BinanceModel
from utils.date.date import get_date_from_utc


def get_data_for_training(pair, start_date, end_date, interval, _client):
    klines = _client.get_historical_klines(pair, intervals[interval], start_date, end_date)
    rows = []
    ATR_object = ATR()

    for _index, row in enumerate(klines):
        open_date, open_time = get_date_from_utc(row[0])

        row.insert(1, open_date)
        row.insert(2, open_time)
        # Cast in float close prices
        row[6] = float(row[6])
        close_date, close_time = get_date_from_utc(row[8])
        row.insert(9, close_date)
        row.insert(10, close_time)
        close = rows[_index - 1][6] if _index > 0 else 0
        current_true_range = true_range(float(row[4]), float(row[5]), float(close))
        current_ATR = ATR_object.current_atr(current_true_range)
        row.append(current_true_range)
        row.append(current_ATR)
        rows.append(row)

    dataframe = pandas.DataFrame(rows, columns=headers)
    file_name = "data/{}.csv".format(pair)

    dataframe.to_csv(file_name)


def get_past_candlesticks(interval, start_time, _pair, _look_back=BinanceModel.LOOK_BACK):

    _rows = []
    _current_time = 0
    binance_client = BinanceModel()
    client = binance_client.get_client()
    candles = client.get_historical_klines(_pair, interval, start_time)
    for row in candles[-_look_back:]:
        open_date, open_time = get_date_from_utc(row[0])

        row.insert(1, open_date)
        row.insert(2, open_time)

        close_date, close_time = get_date_from_utc(row[8])
        row.insert(9, close_date)
        row.insert(10, close_time)
        _rows.append(row)

        _current_time = row[0]
    _rows = [float(_input[6]) for _input in _rows]
    return _rows, _current_time


def get_minutes_from_now(_current_time, _num_minutes):
    """
    List the time of the format 04:42:00 from now up to # _num_minutes
    :param _current_time: int -- current time in utc
    :param _num_minutes: int -- number of minutes to count from now
    :return: list -- list of times of the format 04:42:00
    """
    _time_steps = []
    for _time_step in range(_num_minutes):
        _tmp_time = get_date_from_utc(_current_time + 60000 * (_time_step + 1))[1]

        _time_steps.append(_tmp_time)
    return _time_steps
