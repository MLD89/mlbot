import talib
from talib import MAX, MIN

ROC = talib.abstract.ROC
SMA = talib.abstract.Function('sma')
RSI = talib.abstract.RSI
EMA = talib.abstract.EMA
CCI = talib.abstract.CCI
WILLR = talib.abstract.WILLR
MACD = talib.abstract.MACD
AROONOSC = talib.abstract.AROONOSC
SAR = talib.abstract.SAR


def true_range(current_high, current_low, previous_close=0):
    return max(abs(current_high - current_low),
               abs(current_high - previous_close),
               abs(current_low - previous_close))


class ATR:

    def __init__(self):
        self.true_range_history = []
        self.prior_ATR = -1
        self.periods = 14

    def current_atr(self, current_true_range):
        if len(self.true_range_history) < self.periods:
            self.true_range_history.append(current_true_range)
            current_ATR = -1

        elif self.prior_ATR == -1:
            current_ATR = sum(self.true_range_history) / self.periods

        else:
            current_ATR = ((self.prior_ATR * (self.periods - 1)) + current_true_range) / self.periods

        self.prior_ATR = current_ATR
        return current_ATR


def get_clv(dataframe):
    new_rows = (dataframe['close'].astype(float).values - dataframe['low'].astype(float).values)
    new_rows -= (dataframe['high'].astype(float).values - dataframe['close'].astype(float).values)
    new_rows /= (dataframe['high'].astype(float).values - dataframe['low'].astype(float).values)
    return new_rows


def get_adi(dataframe):
    adi = dataframe['CLV'] * dataframe['Volume'].astype(float)
    return adi


def get_sar(dataframe):
    high = MAX(dataframe['close'].values, timeperiod=5)
    low = MIN(dataframe['close'].values, timeperiod=5)
    parabolic_sar = talib.SAR(high, low, acceleration=0.02, maximum=0.2)[-1]
    return parabolic_sar
