from datetime import datetime

from statics.months import months


def get_date_from_utc(_utc_):
    """
    Get the date and time format from utc int:
                1539060120000 -- > 2018-10-09 , 04:42:00
    :param _utc_: int -- utc int e.g 1539060120000
    :return: tuple(string, string) -- 2018-10-09 , 04:42:00
    """
    _utc_int = int(str(_utc_)[:-3])
    time_string = datetime.utcfromtimestamp(_utc_int).isoformat().split('T')
    date = time_string[0]
    time = time_string[1]

    return date, time


def check_date(_date_string):
    """
    Check if the inputted date
    :param _date_string: string -- user inputted string
    :return: string  or None -- if input is date type, else None
    """
    try:

        date = _date_string.split('/')
        day = date[0]
        month = date[1]
        year = date[2]
        month = months[month].split('/')[0]

        if int(day) > int(months[date[1]].split('/')[1]):
            return None
        date_string = "{} {}, {}".format(day, month, year)
        return date_string
    except Exception:
        return None
