import os

import keras
import tensorflow

from utils.model.model_utils import *

config = tensorflow.ConfigProto(device_count={'GPU': 1, 'CPU': 56})
sess = tensorflow.Session(config=config)
keras.backend.set_session(sess)


def train_quantitative_model(graph, pair):
    # Some prints are hidden
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
    # Read from csv file for training
    data_file_path = 'data/{}.csv'.format(pair)
    # Load train Data
    print('Loading Data ...')
    trainX, trainY, scaler = load_data(data_file_path, _from_file=True, _binary=False)
    print('Data Loaded')
    # Build a model
    with graph.as_default():
        model = build_quantitative_model(output_size=1, neurons=BinanceModel.NEURONS)
        # Train the model
        _ = model.fit(trainX, trainY, epochs=BinanceModel.EPOCHS, batch_size=BinanceModel.BATCH_SIZE, verbose=1,
                      validation_split=0.05, shuffle=False)
        # Save weights of network
        model.save('model/{}_model.h5'.format(pair))
    print("Saved model to disk")


def train_up_down_model(graph, pair):
    # Some prints are hidden
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
    # Read from csv file for training
    data_file_path = 'data/{}.csv'.format(pair)
    # Load train Data
    print('Loading Data ...')
    trainX, trainY, scaler = load_data(data_file_path, _from_file=True, _binary=True)
    print('Data Loaded')
    # Build a model
    with graph.as_default():
        model = build_up_down_model(neurons=BinanceModel.NEURONS)
        # Train the model
        _ = model.fit(trainX, trainY, epochs=BinanceModel.EPOCHS, batch_size=BinanceModel.BATCH_SIZE, verbose=1,
                      validation_split=0.05, shuffle=False)
        # Save weights of network
        model.save('model/{}_binary_model.h5'.format(pair))
    print("Saved model to disk")
