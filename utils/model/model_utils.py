import matplotlib.pyplot as plt
import numpy
import pandas
from keras.layers.core import Dense, Activation
from keras.layers.recurrent import LSTM
from keras.models import Sequential
from sklearn.preprocessing import MinMaxScaler
from configuration import BinanceModel

PRICES_FOR_CHANGING = {'up': [0, 1], 'down': [1, 0]}


def create_dataset(dataset, look_back=1):
    """
    Create a dataset for training; Get # look_back prices together
    :param dataset: numpy array -- all inputs
    :param look_back: int -- number of past actions to use with prediction
    :return: tuple -- inputs and outputs
    """
    dataX, dataY = [], []
    for i in range(len(dataset) - look_back - 1):
        a = dataset[i:(i + look_back), 0]
        dataX.append(a)
        dataY.append(dataset[i + look_back, 0])
    return numpy.array(dataX), numpy.array(dataY)


def create_dataset_up_down(dataset, look_back=1):
    """
    Create a dataset for training; Get # look_back prices together
    :param dataset: numpy array -- all inputs
    :param look_back: int -- number of past actions to use with prediction
    :return: tuple -- inputs and outputs
    """

    samples, goals = [], []
    for i in range(len(dataset) - look_back):
        x = dataset[i:(i + look_back)]
        x = [each[0] for each in x]
        samples.append(x)
        y = dataset[i + look_back]
        changing = PRICES_FOR_CHANGING['up'] if x[0] < y[0] else PRICES_FOR_CHANGING['down']
        goals.append(changing)
    return numpy.array(samples), numpy.array(goals)


def build_quantitative_model(output_size, neurons):
    """
    :param output_size: int -- input size
    :param neurons: int -- number of neurons
    :return: model
    """
    model = Sequential()
    model.add(
        LSTM(neurons, return_sequences=True, input_shape=(BinanceModel.INPUT_FEATURES, BinanceModel.LOOK_BACK),
             activation=BinanceModel.ACTIVATION))
    model.add(LSTM(neurons, return_sequences=True, activation=BinanceModel.ACTIVATION))
    model.add(LSTM(neurons, activation=BinanceModel.ACTIVATION))
    model.add(Dense(units=output_size))
    model.add(Activation(BinanceModel.ACTIVATION))
    model.compile(loss=BinanceModel.LOSS, optimizer=BinanceModel.OPTIMIZER, metrics=['mae'])
    return model


def build_up_down_model(neurons):
    """
    :param neurons: int -- number of neurons
    :return: model
    """
    model = Sequential()
    model.add(
        LSTM(neurons, return_sequences=True, input_shape=(BinanceModel.INPUT_FEATURES, BinanceModel.LOOK_BACK),
             activation=BinanceModel.ACTIVATION))
    model.add(LSTM(neurons, return_sequences=True, activation=BinanceModel.ACTIVATION))
    model.add(LSTM(neurons, activation=BinanceModel.ACTIVATION))
    model.add(Dense(units=2, kernel_initializer='normal', activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer="adam", metrics=['accuracy'])
    return model


def load_data(_data, _from_file=False, _from_list=False, _binary=False):
    """
    Load data from path or list:

    :param _binary:
    :param _from_list: bool -- whether or not from list
    :param _data: string or list
    :param _from_file: bool -- whether or not from file
    :return: pair of list
    """
    stock_prices = []
    if _from_file:
        prices_dataset = pandas.read_csv(_data, header=0)
        stock_prices = prices_dataset.close.values.astype('float32')
    elif not _from_file and not _from_list:
        stock_prices = numpy.array([_input[6] for _input in _data]).astype('float32')

    if _from_list:
        stock_prices = numpy.array(_data).astype('float32')

    stock_prices = stock_prices.reshape(stock_prices.shape[0], 1)

    scaler = MinMaxScaler(feature_range=(0, 1))
    stock_prices = scaler.fit_transform(stock_prices)

    train_size = int(len(stock_prices) * BinanceModel.TRAIN_DATA_PERCENT)
    train = stock_prices[0:train_size, :]
    if not _from_file:
        trainX = numpy.array(train)
        trainX = numpy.reshape(trainX, (trainX.shape[1], 1, trainX.shape[0]))
        return trainX, scaler
    if _binary:
        trainX, trainY = create_dataset_up_down(train, BinanceModel.LOOK_BACK)
    else:
        trainX, trainY = create_dataset(train, BinanceModel.LOOK_BACK)
    trainX = numpy.reshape(trainX, (trainX.shape[0], 1, trainX.shape[1]))
    return trainX, trainY, scaler


def plot_results(predicted_data, true_data, scaler):
    """
    :param scaler:
    :param predicted_data: list
    :param true_data: list
    :return:
    """
    true_data = true_data.reshape(-1, 1)
    plt.plot(scaler.inverse_transform(true_data))
    predicted_data = scaler.inverse_transform(predicted_data)
    plt.plot(predicted_data)
    plt.show()


def predict_sequences_multiple(model, _prediction_set, scaler):
    """
    Predict from multiple inputs
    :param scaler:
    :param model: model
    :param _prediction_set: list
    :return: list -- predictions
    """
    prediction_seqs = model.predict(_prediction_set)
    predicted_data = scaler.inverse_transform(prediction_seqs)

    return predicted_data
