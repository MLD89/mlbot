from binance.client import Client
from flask import Flask


class BinanceModel:
    PAIR = 'ONTETH'
    START_TIME = '16/11/2018'
    END_TIME = '20/11/2018'
    INTERVAL = '1m'
    EPOCHS = 1000
    LOOK_BACK = 25
    NEURONS = 1024
    BATCH_SIZE = 128
    API_KEY = 'ujiL8jglEw1B0VEweUayK4VLKjy6JIOWE76o3TIHxtiexzLNnjVpi68nt4CbxSSx'
    API_SECRET = '1EnkzgiSrcjLjbZGkzGFfc1uL4TKq7qxLO30UzFnjc4QBtAlviIT39Ch81ws4zlU'
    ACTIVATION = 'tanh'
    LOSS = 'mse'
    OPTIMIZER = "adam"
    DROPOUT = 0
    TRAINING_SIZE = 0.8
    TRAIN_DATA_PERCENT = 1
    INPUT_FEATURES = 1
    LAYERS = 2
    MAIN_PAIR = 'ADABNB'

    def __init__(self):
        self.CLIENT = Client(self.API_KEY, self.API_SECRET)

    def get_client(self):
        return self.CLIENT


class ApiModel:
    def __init__(self):
        self.app = Flask(__name__,
                         static_url_path='',
                         static_folder='static',
                         template_folder='templates')
        self.app.config.from_object(__name__)
        self.app.config['SECRET_KEY'] = '7d441f27d441f27567d441f2b6176a'
        self.app.config['DEBUG'] = False

    def get_app(self):
        return self.app
