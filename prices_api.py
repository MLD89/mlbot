import os

import tensorflow as tf
from flask import render_template, request, send_from_directory, url_for, redirect
from keras.models import load_model

from statics.symbols import prediction_intervals, prediction_pairs, intervals, start_time
from utils.data.get_data import get_past_candlesticks
from utils.model.model_utils import load_data
from utils.structures import Stack
from configuration import BinanceModel, ApiModel
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

app = ApiModel().get_app()

# load the model, and pass in the custom metric function
GRAPH = tf.get_default_graph()
PRICE_MODEL = load_model("model/{}_model.h5".format(BinanceModel.MAIN_PAIR))
UP_DOWN_MODEL = load_model("model/{}_binary_model.h5".format(BinanceModel.MAIN_PAIR))


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')


@app.route('/', methods=['POST', 'GET'])
def main():
    if request.method == 'POST':
        pair = request.form['pairs']
        interval = request.form['interval']
        return redirect(url_for('graph', pair=pair, interval=interval))

    return render_template('charts.html', graph=False, pairs=prediction_pairs,
                           intervals=prediction_intervals)


def get_predictions(_interval, pair):
    _num_minutes = prediction_intervals[_interval]
    global GRAPH

    predictions = []
    prediction_indicator = 0
    i = 0
    result, _current_minute = get_past_candlesticks(intervals[_interval], start_time[_interval],
                                                    pair, BinanceModel.LOOK_BACK)

    _static_stack = Stack(result)
    _minutes = list(range(1, _num_minutes + 1))
    while i < 15:
        _input, scaler = load_data(_static_stack.items, _from_list=True)
        if i == 0:
            with GRAPH.as_default():
                prediction_indicator = UP_DOWN_MODEL.predict(_input)
            # prediction_indicator = scaler.inverse_transform(prediction_indicator)
        i += 1
        with GRAPH.as_default():
            prediction_seqs = PRICE_MODEL.predict(_input)
        prediction = scaler.inverse_transform(prediction_seqs)[0][0]
        predictions.append(prediction)
        _static_stack.push(prediction)

    return _minutes, predictions, 'Down' if prediction_indicator[0][0] > prediction_indicator[0][1] else 'Up'


@app.route('/graph/<pair>/<interval>', methods=['POST', 'GET'])
def graph(pair, interval):
    global PRICE_MODEL, GRAPH, UP_DOWN_MODEL
    if request.method == 'POST':
        pair = request.form['pairs']
        interval = request.form['interval']
        return redirect(url_for('graph', pair=pair, interval=interval))
    if not pair == CURRENT_PAIR:
        tf.reset_default_graph()
        with GRAPH.as_default():
            PRICE_MODEL = load_model("model/{}_model.h5".format(pair.strip()))
            UP_DOWN_MODEL = load_model("model/{}_binary_model.h5".format(pair.strip()))

    _time_list, data_set, up_down_indicator = get_predictions(interval, pair)

    return render_template('charts.html', graph=True, time_list=_time_list, data_set=data_set, current_pair=str(pair),
                           pairs=prediction_pairs, intervals=prediction_intervals, up_down=str(up_down_indicator))


if __name__ == "__main__":

    CURRENT_PAIR = BinanceModel.MAIN_PAIR
    binance_model = BinanceModel()
    client = binance_model.CLIENT
    app.run(host='0.0.0.0', port=1725)
