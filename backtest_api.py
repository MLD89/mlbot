import os

import tensorflow as tf
from flask import render_template, request, send_from_directory, url_for, redirect
from keras.models import load_model

from api.forms.date import InsertDateForm
from configuration import BinanceModel, ApiModel
from statics.symbols import prediction_intervals, prediction_pairs
from utils.backtest import get_predictions
from utils.date.date import check_date

app = ApiModel().get_app()
GRAPH = tf.get_default_graph()
PRICE_MODEL = load_model("model/{}_model.h5".format(BinanceModel.MAIN_PAIR))


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')


@app.route('/', methods=['POST', 'GET'])
def main():
    form = InsertDateForm(request.form)
    sorted_symbols = sorted(prediction_pairs)
    if request.method == 'POST':
        start_date = check_date(request.form['start_date'])
        end_date = check_date(request.form['end_date'])
        pair = request.form['pairs']
        interval = request.form['interval']

        return redirect(url_for('backtest', pair=pair, start_date=start_date, end_date=end_date, interval=interval))

    return render_template('backtest.html', form=form, pairs=sorted_symbols, intervals=prediction_intervals)


@app.route('/backtest/<pair>/<start_date>/<end_date>/<interval>', methods=['POST', 'GET'])
def backtest(pair, start_date, end_date, interval):
    if request.method == 'POST':
        start_date = check_date(request.form['start_date'])
        end_date = check_date(request.form['end_date'])
        pair = request.form['pairs']
        interval = request.form['interval']

        return redirect(url_for('backtest', pair=pair, start_date=start_date, end_date=end_date, interval=interval))

    global GRAPH, PRICE_MODEL
    if not pair == CURRENT_PAIR:
        tf.reset_default_graph()
        with GRAPH.as_default():
            PRICE_MODEL = load_model("model/{}_model.h5".format(pair.strip()))
    form = InsertDateForm(request.form)
    sorted_symbols = sorted(prediction_pairs)
    results = get_predictions(interval, start_date, end_date, pair, PRICE_MODEL, GRAPH)
    return render_template('backtest.html', form=form, pairs=sorted_symbols, intervals=prediction_intervals,
                           results=results)


if __name__ == "__main__":
    CURRENT_PAIR = BinanceModel.MAIN_PAIR
    binance_model = BinanceModel()
    client = binance_model.CLIENT
    app.run(host='0.0.0.0', port=1727, debug=False)
