# USAGE #

### python train_model.py ###

* Train pair of binance
* Run python train_model.py

### python cfg.py ###

* Initialize parameters:  
* Choose interval from: '1m' '3m' '5m' '15m' '30m' '1h' '2h' '4h' '6h' '8h' '12h'  '1d' '3d' '1w' '1M'


### python data_api.py ###

* Download data from different pairs of binance
* Runs on: http://127.0.0.1:1723/

### python prices_api.py ###

* Get prices for pairs of binance
* Runs on: http://127.0.0.1:1725/

### python train_api.py ###

* Trains and saves weights for pairs of binance
* Runs on: http://127.0.0.1:1718/

### python backtest_api.py ###

* Gets real value predicted value and absolute loss of historical data
* Runs on: http://127.0.0.1:1727/

## Installing instructions:

* pip install -r requirements.txt
* Download models from <a href="https://drive.google.com/open?id=1ZLvDOlnLWrU7lcS_XdzR5Bj0ejl9Z8jZ">here</a> 
and add them to folder 'model'

"conda install -c developer ta-lib"
