import os

from flask import render_template, request, send_from_directory, url_for, redirect
import tensorflow as tf

from api.forms.date import InsertDateForm
from configuration import BinanceModel, ApiModel
from statics.symbols import prediction_intervals, symbols
from utils.data.get_data import get_data_for_training
from utils.model.train_utils import train_quantitative_model
from utils.model.train_utils import train_up_down_model

app = ApiModel().get_app()
GRAPH = tf.get_default_graph()


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')


@app.route('/', methods=['POST', 'GET'])
def main():
    form = InsertDateForm(request.form)
    sorted_symbols = sorted(symbols)
    if request.method == 'POST':
        start_date = request.form['start_date'].replace('/', '|')
        end_date = request.form['end_date'].replace('/', '|')
        pair = request.form['pairs']
        interval = request.form['interval']

        return redirect(url_for('train', pair=pair, start_date=start_date, end_date=end_date, interval=interval))
    return render_template('train.html', form=form, pairs=sorted_symbols, intervals=prediction_intervals)


@app.route('/train/<pair>/<start_date>/<end_date>/<interval>', methods=['POST', 'GET'])
def train(pair, start_date, end_date, interval):
    global GRAPH
    if request.method == 'POST':
        start_date = request.form['start_date'].replace('/', '|')
        end_date = request.form['end_date'].replace('/', '|')
        pair = request.form['pairs']
        interval = request.form['interval']

        return redirect(url_for('train', pair=pair, start_date=start_date, end_date=end_date, interval=interval))

    start_date = start_date.replace('|', '/')
    end_date = end_date.replace('|', '/')

    binance_model = BinanceModel()
    client = binance_model.CLIENT
    get_data_for_training(pair, start_date, end_date, interval, client)

    train_quantitative_model(GRAPH, pair)
    train_up_down_model(GRAPH, pair)

    form = InsertDateForm(request.form)
    sorted_symbols = sorted(symbols)
    return render_template('train.html', form=form, pairs=sorted_symbols, intervals=prediction_intervals)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=1718, debug=False)
